package ajedrez;

import java.util.ArrayList;

import javax.swing.ImageIcon;

public class Torre extends Pieza {
	public Torre(Celda celda, Equipo equipo) {
		super(celda,equipo);
		if(equipo.isEstaArriba())
			this.setIcono(new ImageIcon("D:\\Users\\corre\\eclipse-workspace\\TrabajoPracticoIntegrador\\img\\torreNegra.png"));
		else
			this.setIcono(new ImageIcon("D:\\Users\\corre\\eclipse-workspace\\TrabajoPracticoIntegrador\\img\\torreBlanca.png"));
	}


	@Override
	public ArrayList<Celda> getMovimientosPosibles() {
		ArrayList<Celda> posiblesMovimientos=new ArrayList<Celda>();// se crea un array para poder incluir posibles jugadas
		Tablero tablero= new Tablero();
		tablero=this.getEquipo().getAjedrez().getTablero();
		int fila=this.getCelda().getFila(); // se desglosa la celda que contiene al peon en fila y columna
		int columna=this.getCelda().getColumna();
		Celda celdaAux=new Celda(0,0); // se crea una celda para manipular casos

		boolean sePuede=true;
		int i=fila;
		while(sePuede) { //mientras se puede avanzar hacia arriba 
			if (i>0) {
				i--;
				celdaAux=tablero.getCelda(i, columna);
				if(celdaAux.puedeIngresarPieza(this)) {
					posiblesMovimientos.add(celdaAux);
				}
				else {
					sePuede=false;
				}
			}
			else {
				sePuede=false;
			}
		}

		sePuede=true;
		i=fila;
		while(sePuede) { //mientras se puede avanzar hacia abajo 
			if (i<7) {
				i++;
				celdaAux=tablero.getCelda(i, columna);
				if(celdaAux.puedeIngresarPieza(this)) {
					posiblesMovimientos.add(celdaAux);
				}
				else {
					sePuede=false;
				}
			}
			else {
				sePuede=false;
			}
		}

		sePuede=true;
		i=columna;
		while(sePuede) { //mientras se puede avanzar hacia derecha
			if (i<7) {
				i++;
				celdaAux=tablero.getCelda(fila, i);
				if(celdaAux.puedeIngresarPieza(this)) {
					posiblesMovimientos.add(celdaAux);
				}
				else {
					sePuede=false;
				}
			}
			else {
				sePuede=false;
			}
		}

		sePuede=true;
		i=columna;
		while(sePuede) { //mientras se puede avanzar hacia izquierda 
			if (i>0) {
				i--;
				celdaAux=tablero.getCelda(fila, i);
				if(celdaAux.puedeIngresarPieza(this)) {
					posiblesMovimientos.add(celdaAux);
				}
				else {
					sePuede=false;
				}
			}
			else {
				sePuede=false;
			}
		}





		return posiblesMovimientos;
	}


	@Override
	public String nombrePieza() {
		return "Torr";
	}
}
