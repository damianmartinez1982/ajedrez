package ajedrez;

import java.util.ArrayList;

import javax.swing.ImageIcon;

public class Rey extends Pieza {
	public Rey(Celda celda, Equipo equipo) {
		super(celda,equipo);
		if(equipo.isEstaArriba())
			this.setIcono(new ImageIcon("D:\\Users\\corre\\eclipse-workspace\\TrabajoPracticoIntegrador\\img\\reyNegro.png"));
		else
			this.setIcono(new ImageIcon("D:\\Users\\corre\\eclipse-workspace\\TrabajoPracticoIntegrador\\img\\reyBlanco.png"));
	}

	@Override
	public ArrayList<Celda> getMovimientosPosibles() {
		ArrayList<Celda> posiblesMovimientos=new ArrayList<Celda>();// se crea un array para poder incluir posibles jugadas
		Tablero tablero=this.getEquipo().getAjedrez().getTablero();
		int fila=this.getCelda().getFila(); // se desglosa la celda que contiene al rey en fila y columna
		int columna=this.getCelda().getColumna();
		Celda celda=new Celda(fila,columna); // se crea una celda para manipular casos

		for(int i=-1;i<=1;i++) {
			for(int j=-1;j<=1;j++) {
				if (fila+i>=0 && fila+i<=7) {
					if (columna+j>=0 && columna+j<=7) {
						celda.setFila(fila+i);celda.setColumna(columna+j);
						if(tablero.getCelda(fila+i, columna+j).getPieza()==null) {
							posiblesMovimientos.add(celda);
						}else if (tablero.getCelda(fila+i, columna+j).estaOcupadaEquipoContrario(this.getEquipo())) {
							posiblesMovimientos.add(celda);						
						}
					}
				}
			}
		}

		return posiblesMovimientos;
	}



	@Override
	public String nombrePieza() {
		return "Rey ";
	}
}
