package ajedrez;

import java.util.ArrayList;

import javax.swing.Icon;

public abstract class Pieza{
	private Celda celda;
	private Equipo equipo; 
	private boolean estaViva;
	private Icon icono;
	private ArrayList<IPiezaListener> piezaListeners;
	

	public Pieza(Celda celda, Equipo equipo) {
		this.celda=celda;
		this.equipo=equipo;
		this.estaViva=true;
		this.piezaListeners=new ArrayList<IPiezaListener>();
	}
	
		
	public Celda getCelda() {
		return this.celda;
	}


	public void setCelda(Celda celda) {
		for (IPiezaListener escuchador : piezaListeners) {
			escuchador.piezaMovida(this, this.celda, celda);
		}
		this.celda = celda;
	}


	public Equipo getEquipo() {
		return equipo;
	}


	public void setEquipo(Equipo equipo) {
		this.equipo = equipo;
	}


	public boolean isEstaViva() {
		return estaViva;
	}


	public void setEstaViva(boolean estaViva) {
		for (IPiezaListener escuchador : piezaListeners) {
			escuchador.piezaComida(this, this.celda, celda);
		}
		this.estaViva = estaViva;
	}


	public ArrayList<Celda> getMovimientosPosibles(){
		ArrayList<Celda> movimientos=null;
		return movimientos;
	}
	public String nombrePieza() {
		return null;
	}


	public Icon getIcono() {
		return icono;
	}


	public void setIcono(Icon icono) {
		this.icono = icono;
	}
	
	public void removePiezaListener(IPiezaListener listener) {
		this.piezaListeners.remove(listener); 
	}
	
	public void addPiezaListener(IPiezaListener listener) {
		this.piezaListeners.add(listener);  
	}
	
	

}
