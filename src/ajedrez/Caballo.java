package ajedrez;

import java.util.ArrayList;

import javax.swing.ImageIcon;

public class Caballo extends Pieza {
	public Caballo(Celda celda, Equipo equipo) {
		super(celda, equipo);
		if(equipo.isEstaArriba())
			this.setIcono(new ImageIcon("D:\\Users\\corre\\eclipse-workspace\\TrabajoPracticoIntegrador\\img\\caballoNegro.png"));
		else
			this.setIcono(new ImageIcon("D:\\Users\\corre\\eclipse-workspace\\TrabajoPracticoIntegrador\\img\\caballoBlanco.png"));
	}

	@Override
	public String nombrePieza() {
		return "Caba";
	}

	@Override
	public ArrayList<Celda> getMovimientosPosibles() {
		ArrayList<Celda> posiblesMovimientos = new ArrayList<Celda>();
		Tablero tablero;
		tablero=this.getEquipo().getAjedrez().getTablero();
		// por derecha
		Celda celdaAux;
        //cada if corresponde a un movimiento posible del caballo        
		if ((this.getCelda().getFila() + 2 <=7 ) &&(this.getCelda().getColumna() + 1 <=7)) {
			//aseguramos que los margenes sean correctos
			celdaAux = tablero.getCelda(this.getCelda().getFila() + 2, this.getCelda().getColumna() + 1);
			if (celdaAux.puedeIngresarPieza(celdaAux.getPieza())) {
				posiblesMovimientos.add(celdaAux);
			} else {
				if (celdaAux.estaOcupadaEquipoContrario(this.getEquipo())) {
					posiblesMovimientos.add(celdaAux);
		          }
				
			}
		}
		if ((this.getCelda().getFila() + 2 <=7) && (this.getCelda().getColumna() - 1 >=0)) {
			celdaAux = tablero.getCelda(this.getCelda().getFila() + 2, this.getCelda().getColumna()-1);
			if (celdaAux.puedeIngresarPieza(celdaAux.getPieza())) {
				posiblesMovimientos.add(celdaAux);
			} else {
				if (celdaAux.estaOcupadaEquipoContrario(this.getEquipo())) {
					posiblesMovimientos.add(celdaAux);
		          }
				
			}
		}
		
		
		if (((this.getCelda().getFila() -2) >=0 ) && ((this.getCelda().getColumna() + 1) <=7)) {
			celdaAux = tablero.getCelda((this.getCelda().getFila() -2),( this.getCelda().getColumna() +1));
			if (celdaAux.puedeIngresarPieza(celdaAux.getPieza())) {
				posiblesMovimientos.add(celdaAux);
			} else {
				if (celdaAux.estaOcupadaEquipoContrario(this.getEquipo())) {
					posiblesMovimientos.add(celdaAux);
		          }
				
			}
		}
		if ((this.getCelda().getFila() - 2 >=0) && (this.getCelda().getColumna() - 1 >=0)) {
			celdaAux = tablero.getCelda(this.getCelda().getFila() - 2, this.getCelda().getColumna()-1);
			if (celdaAux.puedeIngresarPieza(celdaAux.getPieza())) {
				posiblesMovimientos.add(celdaAux);
			} else {
				if (celdaAux.estaOcupadaEquipoContrario(this.getEquipo())) {
					posiblesMovimientos.add(celdaAux);
		          }
		 
			 
		 }
		}
		if ((this.getCelda().getFila() + 1 <=7 ) && (this.getCelda().getColumna() -2 >=0)) {
			celdaAux = tablero.getCelda(this.getCelda().getFila() + 1, this.getCelda().getColumna() -2);
			if (celdaAux.puedeIngresarPieza(celdaAux.getPieza())) {
				posiblesMovimientos.add(celdaAux);
			} else {
				if (celdaAux.estaOcupadaEquipoContrario(this.getEquipo())) {
					posiblesMovimientos.add(celdaAux);
		          }
				
			}
		}
		if ((this.getCelda().getFila() -1 >=0) && (this.getCelda().getColumna() - 2 >=0)) {
			celdaAux = tablero.getCelda(this.getCelda().getFila() - 1, this.getCelda().getColumna()- 2);
			if (celdaAux.puedeIngresarPieza(celdaAux.getPieza())) {
				posiblesMovimientos.add(celdaAux);
			} else {
				if (celdaAux.estaOcupadaEquipoContrario(this.getEquipo())) {
					posiblesMovimientos.add(celdaAux);
		          }
				
			}
		}
		if ((this.getCelda().getFila() + 1 <= 7) && (this.getCelda().getColumna() + 2 <=7)) {
			celdaAux = tablero.getCelda(this.getCelda().getFila() + 1, this.getCelda().getColumna() + 2);
			if (celdaAux.puedeIngresarPieza(celdaAux.getPieza())) {
				posiblesMovimientos.add(celdaAux);
			} else {
				if (celdaAux.estaOcupadaEquipoContrario(this.getEquipo())) {
					posiblesMovimientos.add(celdaAux);
		          }
				
			}
		}
		if ((this.getCelda().getFila() -1 >=0 ) && (this.getCelda().getColumna() + 2 <=7)) {
			celdaAux = tablero.getCelda(this.getCelda().getFila() -1, this.getCelda().getColumna()  +2);
			if (celdaAux.puedeIngresarPieza(celdaAux.getPieza())) {
				posiblesMovimientos.add(celdaAux);
			} else {
				if (celdaAux.estaOcupadaEquipoContrario(this.getEquipo())) {
					posiblesMovimientos.add(celdaAux);
		          }
				
			}
		}
		
	
		
	 return posiblesMovimientos;
	}
}
