package ajedrez;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.border.LineBorder;

public class CeldaGUI extends JButton{
	private Icon icono;
	private Color colorEncima;
	private Color colorCelda;
	private int fila;
	private int columna;
	
	CeldaGUI(Color colorCelda){
		this.colorEncima=Color.DARK_GRAY;
		this.setBorder(new LineBorder(colorEncima));
		this.setBackground(colorCelda);
		this.setMaximumSize(new Dimension(64, 64));
		this.setMinimumSize(new Dimension(64, 64));
		this.setPreferredSize(new Dimension(40, 40));
		this.setVisible(true);
	}

	public Icon getIcono() {
		return icono;
	}

	public void setIcono(Icon icono) {
		this.icono = icono;
	}

	public Color getColorEncima() {
		return colorEncima;
	}

	public void setColorEncima(Color colorEncima) {
		this.colorEncima = colorEncima;
	}
	public void unsetColorEncima() {
		this.colorEncima = Color.DARK_GRAY;
	}
	
	public int getFila() {
		return fila;
	}

	public void setFila(int fila) {
		this.fila = fila;
	}

	public int getColumna() {
		return columna;
	}

	public void setColumna(int columna) {
		this.columna = columna;
	}


}
