package ajedrez;



public class Celda {
	private int fila;
	private int columna;
	private Pieza pieza;

	public Celda(int fila, int columna) {
		this.fila=fila;
		this.columna=columna;
		this.pieza=null;
	}
	public Celda(int fila, int columna, Pieza pieza) {
		this(fila,columna);
		this.pieza=pieza;
	}


	public int getFila() {
		return fila;
	}
	public void setFila(int fila) {
		this.fila = fila;
	}
	public int getColumna() {
		return columna;
	}
	public void setColumna(int columna) {
		this.columna = columna;
	}
	public Pieza getPieza() {
		return pieza;
	}
	public void setPieza(Pieza pieza) {
		this.pieza = pieza;
	}
	public boolean puedeIngresarPieza(Pieza pieza) {
		if (this.pieza==null || this.estaOcupadaEquipoContrario(pieza.getEquipo()))
			return true;
		else
			return false;

	}
	/*public boolean esCeldaTablero() {
		boolean esDelTablero=false;
		if(this.getFila()>=0 && this.getFila()<=7) {
			if(this.getColumna()>=0 && this.getColumna()<=7) {
				esDelTablero=true;
			}
		}
		return esDelTablero;
	}*/

	public boolean estaOcupadaEquipoContrario(Equipo equipo) {
		boolean esOtroEquipo = false;
		if (this.getPieza()!=null) {
			if (this.getPieza().getEquipo()!=equipo) {
				esOtroEquipo = true;
			}
		}
		return esOtroEquipo;
	}

@Override
public boolean equals(Object obj) {
	if (obj instanceof Celda) {

		Celda celda = (Celda) obj;

		return this.getColumna()== celda.getColumna() && this.getFila()==celda.getFila();
	} else {
		return super.equals(obj);
	
	}
}
}