package ajedrez;

import java.util.ArrayList;

public class Tablero {
	private Celda[][] celdas;

	public void crear() {
		this.celdas = new Celda[8][8];
		for (int i=0;i<=7;i++) {
			for(int j=0;j<=7;j++) {
				Celda auxiliar= new Celda(i,j);
				this.celdas[i][j]=auxiliar;
			}
		}
	}


	public Celda getCelda(int fila, int columna) {
		return this.celdas[fila][columna];
	}

	public void setCelda(int fila, int columna, Celda celda) {
		this.celdas[fila][columna]=celda;
	}

	public void mover(Pieza pieza, int fila, int columna) {
		// aca va el mensaje de la pieza que se mueve....

		int fila0=pieza.getCelda().getFila();
		int columna0=pieza.getCelda().getColumna();
		if (this.getCelda(fila, columna).getPieza()==null) {
			this.getCelda(fila, columna).setPieza(pieza);
			this.getCelda(fila0, columna0).setPieza(null);
			pieza.setCelda(this.getCelda(fila,columna));
			System.out.println(" ");
			System.out.println("La pieza " + pieza.nombrePieza()+" "+ pieza.getEquipo().getNombre()+" se ha movido desde "+ this.getNomenclatura(this.getCelda(fila0, columna0))+ " hasta "+ this.getNomenclatura(this.getCelda(fila, columna)));
			System.out.println(" ");
		pieza.getEquipo().sumarMovimientosRealizados();
		}
		else {
			String nombrePiezaComida = this.getCelda(fila, columna).getPieza().nombrePieza();
			String nombreEquipo=this.getCelda(fila, columna).getPieza().getEquipo().getNombre();
			this.getCelda(fila, columna).getPieza().setEstaViva(false);
			Equipo equipo= this.getCelda(fila, columna).getPieza().getEquipo();
			equipo.actualizarEquipo();
			this.getCelda(fila, columna).setPieza(pieza);
			this.getCelda(fila0, columna0).setPieza(null);
			pieza.setCelda(this.getCelda(fila,columna));
			pieza.getEquipo().sumarPiezasComidas();
			pieza.getEquipo().sumarMovimientosRealizados();
			System.out.println(" ");
			System.out.println("La pieza " + pieza.nombrePieza()+" "+ pieza.getEquipo().getNombre()+
					" se ha movido desde "+ this.getNomenclatura(this.getCelda(fila0, columna0))+ 
					" hasta "+ this.getNomenclatura(this.getCelda(fila, columna))+ " y ha comido a "+nombrePiezaComida+" "+nombreEquipo );
			System.out.println(" ");
			pieza.getEquipo().sumarMovimientosRealizados();
			pieza.getEquipo().sumarPiezasComidas();
		}
	}
	
	
	public String getNomenclatura(Celda celda) {
		// nomenclatura de ajedrez para hacer salidas sin graficos
		int fila=celda.getFila();
		int columna=celda.getColumna();
		String[] auxnomenclatura={"a","b","c","d","e","f","g","h"};

		String nomenclatura[][]=new String[8][8];
		for (int j=0; j<=7; j++) {
			for (int i=7;i>=0;i--) {
				nomenclatura[i][j]=String.format("%s%s",auxnomenclatura[j],(i+1));
			}
		}
		return nomenclatura[fila][columna];
	}

	public void mostrarTablero(){
		System.out.println(" ");
		System.out.println(" ");
		for (int a=0; a<=7; a++) {
			for (int b=0;b<=7;b++) {
				if (this.getCelda(a, b).getPieza()==null) {
					System.out.print("  "+this.getNomenclatura(this.getCelda(a, b))+"  ");
				}
				else {
					System.out.print(" "+this.getCelda(a, b).getPieza().nombrePieza()+" ");
				}
			}
			System.out.println(" ");
		}
		System.out.println(" ");
		System.out.println(" ");
	}

	public ArrayList<Pieza> quienesMatan(Pieza pieza){		
		ArrayList<Pieza> arrayPiezasQueMatan= new ArrayList<Pieza>();		
		Equipo equipo=pieza.getEquipo().getAjedrez().getEquipoContrario(pieza.getEquipo());
		ArrayList<Jugada> jugadasEvaluar= equipo.calcularJugadasPosibles();	
		if(jugadasEvaluar.size()!=0) {
		for(int i=0;i<jugadasEvaluar.size();i++) {
			Celda celda= new Celda(jugadasEvaluar.get(i).getFila(),jugadasEvaluar.get(i).getColumna());
			if(celda.equals(pieza.getCelda())){
				arrayPiezasQueMatan.add(jugadasEvaluar.get(i).getPieza());
			}
		}
		}
		return arrayPiezasQueMatan;
	}

	public boolean esCeldaDelTablero(Celda celda) {
		try {
			this.getCelda(celda.getFila(),celda.getColumna());
			return true;
		} catch (ArrayIndexOutOfBoundsException e) {
			return false;
		}
	}
	
	public ArrayList<Pieza> getPiezas(Equipo equipo) {
		return equipo.getPiezas();
	}

	public void limpiar() {

	}

}