package ajedrez;



import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import java.awt.GridLayout;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.JRadioButton;
import javax.swing.JSplitPane;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;

public class TableroGUI extends JFrame implements IPiezaListener,IJuegoListener{
	private final JPanel panel = new JPanel();
	private CeldaGUI[][] celdasGraficas=new CeldaGUI[8][8];
	private JButton btnTurnoBlancas,btnTurnoNegras,jaquePanelNegras,jaquePanelBlancas;


	public TableroGUI() {
		getContentPane().setBackground(Color.WHITE);
		setMinimumSize(new Dimension(800, 800));
		setMaximumSize(new Dimension(800, 800));
		setVisible(true);
		getContentPane().setMinimumSize(new Dimension(800, 800));
		getContentPane().setFont(new Font("Times New Roman", Font.PLAIN, 13));
		setFont(new Font("Brush Script MT", Font.PLAIN, 12));
		setResizable(false);
		setTitle("TP Integrador \r\nAjedrez POO 2018");
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\corre\\Desktop\\chess_icon-icons.com_65163.png"));
		getContentPane().setMaximumSize(new Dimension(800, 800));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 816, 541);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu menuJuego = new JMenu("Juego");
		menuBar.add(menuJuego);

		JMenu menuDesplegableJugadores = new JMenu("Jugadores");
		menuJuego.add(menuDesplegableJugadores);

		JRadioButton rdbtnNewRadioButton = new JRadioButton("Computadora vs Computadora");
		menuDesplegableJugadores.add(rdbtnNewRadioButton);

		JRadioButton rdbtnHumanoVsComputer = new JRadioButton("Humano vs Computadora");
		menuDesplegableJugadores.add(rdbtnHumanoVsComputer);
		rdbtnHumanoVsComputer.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showConfirmDialog(null, " SI- Para Seleccionar Blancas \n NO- Para Seleccionar Negras \n CANCELAR- Para Salir de este Menu");
			}
		});


		JMenuItem menuIniciar = new JMenuItem("Iniciar");
		menuJuego.add(menuIniciar);

		JMenuItem menuFinalizar = new JMenuItem("Finalizar");
		menuJuego.add(menuFinalizar);

		JMenuItem menuReiniciar = new JMenuItem("Reiniciar");
		menuJuego.add(menuReiniciar);

		JMenuItem menuCerrar = new JMenuItem("Cerrar");
		menuJuego.add(menuCerrar);
		menuCerrar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg) {
				JOptionPane.showMessageDialog(null, "Presione Aceptar Para Salir del Juego", "Gracias por Jugar Ajedrez...", 1);
				System.exit(0);

			}
		});


		JMenu menuAyuda = new JMenu("Ayuda");
		menuBar.add(menuAyuda);

		JMenuItem menuItemSobreJuego = new JMenuItem("Sobre el Juego...");
		menuItemSobreJuego.setSelectedIcon(null);
		menuItemSobreJuego.setSelected(true);
		menuItemSobreJuego.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "POO 2018 - UNPSJB \n Alumnos \n * Martinez Damian Andres\n * Lucero Chuque Carlos Francisco");
			}
		});

		menuAyuda.add(menuItemSobreJuego);
		getContentPane().setLayout(null);
		panel.setPreferredSize(new Dimension(600, 600));
		panel.setSize(new Dimension(600, 600));
		panel.setMinimumSize(new Dimension(600, 600));
		panel.setMaximumSize(new Dimension(600, 600));
		panel.setBorder(new LineBorder(new Color(0, 0, 0), 4));
		panel.setBounds(128, 115, 554, 508);



		/// creacion de botones de turno
		this.btnTurnoBlancas = new JButton("TURNO BLANCAS");
		this.btnTurnoBlancas.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		this.btnTurnoBlancas.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 25));
		this.btnTurnoBlancas.setForeground(Color.BLACK);
		this.btnTurnoBlancas.setBounds(128, 13, 258, 89);
		getContentPane().add(this.btnTurnoBlancas);

		this.btnTurnoNegras = new JButton("TURNO NEGRAS");
		this.btnTurnoNegras.setForeground(Color.BLACK);
		this.btnTurnoNegras.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 25));
		this.btnTurnoNegras.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		this.btnTurnoNegras.setBounds(424, 13, 258, 89);
		getContentPane().add(this.btnTurnoNegras);



		this.jaquePanelBlancas = new JButton("");
		jaquePanelBlancas.setBorder(new LineBorder(Color.RED, 4, true));
		this.jaquePanelBlancas.setIcon(new ImageIcon("D:\\Users\\corre\\eclipse-workspace\\TrabajoPracticoIntegrador\\img\\warningIcon.png"));
		this.jaquePanelBlancas.setVisible(false);
		this.jaquePanelBlancas.setForeground(Color.WHITE);
		this.jaquePanelBlancas.setBounds(12, 13, 97, 89);
		getContentPane().add(this.jaquePanelBlancas);

		this.jaquePanelNegras = new JButton("");
		jaquePanelNegras.setBorder(new LineBorder(Color.RED, 4, true));
		this.jaquePanelNegras.setIcon(new ImageIcon("D:\\Users\\corre\\eclipse-workspace\\TrabajoPracticoIntegrador\\img\\warningIcon.png"));
		this.jaquePanelNegras.setVisible(false);
		this.jaquePanelNegras.setBounds(701, 13, 97, 89);
		getContentPane().add(this.jaquePanelNegras);



		// creacion de celdas graficas

		getContentPane().add(panel);
		panel.setLayout(new GridLayout(8, 8, 0, 0));
		for(int i=0;i<=7;i++) {
			for(int j=0;j<=7;j++) {
				if(i==0 || i%2==0) {
					if (j==0 || j%2==0) {
						celdasGraficas[i][j]= new CeldaGUI(Color.WHITE);
					}
					else {
						celdasGraficas[i][j]= new CeldaGUI(Color.GRAY);
					}
				}
				else {
					if (j==0 || j%2==0) {
						celdasGraficas[i][j]= new CeldaGUI(Color.GRAY);
					}
					else {
						celdasGraficas[i][j]= new CeldaGUI(Color.WHITE);
					}

				}
				celdasGraficas[i][j].setFila(i);
				celdasGraficas[i][j].setColumna(j);
				celdasGraficas[i][j].setVisible(true);
				panel.add(celdasGraficas[i][j]);
				panel.repaint();
			}
		}
	}


	public CeldaGUI getCeldaGUI(int fila, int columna) {
		return this.celdasGraficas[fila][columna];
	}

	public void setCeldaGUI(int fila, int columna, CeldaGUI celda) {
		this.celdasGraficas[fila][columna]=celda;
	}

	@Override
	public void piezaComida(Pieza pieza, Celda celda1, Celda celda2) {
		this.celdasGraficas[celda1.getFila()][celda1.getColumna()].setIcon(null);
		this.celdasGraficas[celda2.getFila()][celda2.getColumna()].setIcon(pieza.getIcono());
		this.repaint();
	}

	@Override
	public void piezaMovida(Pieza pieza, Celda celda1, Celda celda2) {
		this.celdasGraficas[celda1.getFila()][celda1.getColumna()].setIcon(null);
		this.celdasGraficas[celda2.getFila()][celda2.getColumna()].setIcon(pieza.getIcono());
		this.repaint();
	}
	@Override
	public void turnoActual(Equipo equipo) {
		if (equipo.isEstaArriba()) {
			this.btnTurnoBlancas.setBackground(null);
			this.btnTurnoNegras.setBackground(Color.MAGENTA);


		}
		else {
			this.btnTurnoNegras.setBackground(null);
			this.btnTurnoBlancas.setBackground(Color.MAGENTA);
		}

	}


	@Override
	public void equipoEnJaque(Equipo equipo) {
		if (equipo.isEstaArriba()) {
			this.jaquePanelNegras.setVisible(true);
			
		}
		else {
			this.jaquePanelBlancas.setVisible(true);
		}

	}



}


