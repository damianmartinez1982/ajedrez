package ajedrez;

public interface IJuegoListener {

	public default void equipoEnJaque(Equipo equipo) {
		
	}
	
	public default void turnoActual(Equipo equipo) {
		
	}
	
	public default void piezaComida(Pieza pieza) {
		
	}
	
	
	public default void juegoIniciado() {
		
	}
	
	public default void juegoFinalizado() {
		
	}
}
