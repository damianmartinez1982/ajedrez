package ajedrez;

import java.awt.EventQueue;
import java.awt.PageAttributes;
import java.util.ArrayList;

public class Ajedrez implements IJuegoListener{
	private Tablero tablero;
	private TableroGUI tableroGrafico;
	private Equipo blancas;
	private Equipo negras;
	ArrayList<IJuegoListener> juegoListener;

	public Ajedrez() {
		this.juegoListener=new ArrayList<IJuegoListener>();
		
	}
	public void iniciarJuego() {
		this.tablero=new Tablero();


		// Creando Equipos
		this.blancas = new Equipo(false);
		this.blancas.setAjedrez(this);
		this.blancas.setNombre("blancas");
		this.blancas.setEstaArriba(false);
		this.negras = new Equipo(true);
		this.negras.setAjedrez(this);
		this.negras.setNombre("negras");
		this.negras.setEstaArriba(true);


		//Creando Tableros
		this.tablero.crear();

		try {
			this.tableroGrafico = new TableroGUI();
			tableroGrafico.setVisible(true);
			this.juegoListener.add(tableroGrafico);
		} catch (Exception e) {
			e.printStackTrace();
		}


		// Piezas 		
		this.crearPiezas(this.negras);
		System.out.println("Se han creado las piezas negras...");
		this.crearPiezas(this.blancas);
		System.out.println("Se han creado las piezas blancas...");
	}


	private void crearPiezas(Equipo equipo) {
		// se crea un arrayList para contener las piezas que posteriormente se incluiran en el tablero

		//ArrayList<Pieza> arrayPiezas= new ArrayList<Pieza>();
		Celda celda=new Celda(0, 0);
		Torre torre1=new Torre(celda,equipo);Torre torre2=new Torre(celda,equipo);
		Alfil alfil1=new Alfil(celda,equipo);Alfil alfil2=new Alfil(celda,equipo);
		Caballo caballo1=new Caballo(celda, equipo);Caballo caballo2=new Caballo(celda, equipo);
		Rey rey= new Rey(celda,equipo);
		Dama reina=new Dama(celda,equipo);

		// agregan las piezas a un  arrayList

		equipo.agregarPiezas(torre1);equipo.agregarPiezas(caballo1);equipo.agregarPiezas(alfil1);equipo.agregarPiezas(reina);equipo.agregarPiezas(rey);
		equipo.agregarPiezas(alfil2);equipo.agregarPiezas(caballo2);equipo.agregarPiezas(torre2);
		for(int a=0;a<=7;a++) {
			Peon peon=new Peon(celda,equipo);
			equipo.agregarPiezas(peon);
		}

		if (equipo==this.negras){
			int contador=0;
			for(int i=0;i<=1;i++) {
				for(int j=0; j<=7;j++) {
					equipo.getPiezas().get(contador).addPiezaListener(this.tableroGrafico);
					this.tablero.getCelda(i, j).setPieza(equipo.getPiezas().get(contador));
					equipo.getPiezas().get(contador).setCelda(this.tablero.getCelda(i, j));
					contador++;
				}
			}
			
		}
		else {
			equipo.getPiezas().set(3, rey);
			equipo.getPiezas().set(4, reina);
			int contador=15;
			for(int i=6;i<=7;i++) {
				for(int j=0; j<=7;j++) {
					equipo.getPiezas().get(contador).addPiezaListener(this.tableroGrafico);
					tablero.getCelda(i, j).setPieza(equipo.getPiezas().get(contador));
					equipo.getPiezas().get(contador).setCelda(tablero.getCelda(i, j));
					contador--;
				}

			}
		}
		// misteriosamente me borraba la torre negra, asi es que con estas lineas logro forzar que aparezca en la parte grafica
		this.tableroGrafico.getCeldaGUI(0,0).setIcon(tablero.getCelda(0, 0).getPieza().getIcono());
		this.tableroGrafico.repaint();
		System.out.println("");
	}



	public void comenzar() {
		this.iniciarJuego();
		this.tablero.mostrarTablero();
		while(!this.esFinJuego()) {
			this.turno(this.blancas);
			this.tablero.mostrarTablero();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (!this.esFinJuego()) {
				this.turno(this.negras);
				this.tablero.mostrarTablero();
				
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}


	}




	private void turno(Equipo equipo) {
		
		if (!equipo.estaEnJaque()) {
			for (IJuegoListener escuchador : this.juegoListener) {
				escuchador.turnoActual(equipo);
			}
			Jugada jugada=equipo.jugar();
			Pieza pieza=jugada.getPieza();
			int fila=jugada.getFila();
			int columna=jugada.getColumna();
			this.tablero.mover(pieza, fila, columna);
			equipo.actualizarEquipo();
		}
		else {
			for (IJuegoListener escuchador : this.juegoListener) {
				escuchador.equipoEnJaque(equipo);
			}
			System.out.printf("El equipo %s, esta en JAQUE!!!! \n",equipo.getNombre());
			System.out.printf("El equipo %s, realizo %d movimientos!!!! \n",equipo.getNombre(),equipo.getMovimientosRealizados());
			System.out.printf("El equipo %s, comio %d fichas!!!! \n",equipo.getNombre(),equipo.getPiezasComidas());
			System.out.printf("El equipo %s, realizo %d movimientos!!!! \n",this.getEquipoContrario(equipo).getNombre(),this.getEquipoContrario(equipo).getMovimientosRealizados());
			System.out.printf("El equipo %s, comio %d fichas!!!! \n",this.getEquipoContrario(equipo).getNombre(),this.getEquipoContrario(equipo).getPiezasComidas());

			this.finalizar();
		}
	}



	private boolean esFinJuego() {

		return false;
	}

	public void finalizar() {
		try {
			Thread.sleep(20000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Equipo getGanador() {
		Equipo equipoGanador=null;
		return equipoGanador;
	}

	public void actualizarEquipo(Equipo equipo){
		int largo=equipo.getPiezas().size();
		for (int i=0;i<largo;i++) {
			if(equipo.getPiezas().get(i).isEstaViva()==false && equipo.getPiezas().get(i)==null) {
				equipo.getPiezas().remove(i);
			}
		}
	}

	public Equipo getEquipoContrario(Equipo equipo) {
		if(equipo.getNombre()=="negras")
			return this.blancas;

		else
			return this.negras;
	}

	public Tablero getTablero() {
		return this.tablero;
	}

	public TableroGUI getTableroGrafico() {
		return tableroGrafico;
	}



}

