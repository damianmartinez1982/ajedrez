package ajedrez;

import java.util.ArrayList;

import javax.swing.ImageIcon;

public class Dama extends Pieza {
	public Dama(Celda celda, Equipo equipo) {
		super(celda,equipo);
		if(equipo.isEstaArriba())
			this.setIcono(new ImageIcon("D:\\Users\\corre\\eclipse-workspace\\TrabajoPracticoIntegrador\\img\\reinaNegra.png"));
		else
			this.setIcono(new ImageIcon("D:\\Users\\corre\\eclipse-workspace\\TrabajoPracticoIntegrador\\img\\reinaBlanca.png"));
	}
	@Override
	public String nombrePieza() {
		return "Dama";
	}
	@Override
	public ArrayList<Celda> getMovimientosPosibles() {
		ArrayList<Celda> posiblesMovimientos=new ArrayList<Celda>();
		//por derecha
		boolean celdalibre=true;
		Tablero tablero= new Tablero();
		tablero=this.getEquipo().getAjedrez().getTablero();
		int i=1;
		Celda celdaAux= new Celda(0,0);
		int fila=this.getCelda().getFila(); 
		int columna=this.getCelda().getColumna();
		

		
		while (celdalibre ){  //movimiento disponibles por diagonal derecho superior

			if((fila+i >7) || columna+i>7){
				celdalibre=false; // Condicion no exeda margenes del tablero
			}
			else {
				celdaAux=tablero.getCelda(fila+i,columna+i);
				if (celdaAux.puedeIngresarPieza(this)){
					posiblesMovimientos.add(celdaAux);
				}
				else {
					celdalibre=false;
				}
			}
			i++;
		}

		celdalibre=true;
		i=1;
		while (celdalibre ){ //movimiento disponibles por diagonal izquierdo superior

			if(fila+i >7 || columna-i<0){
				celdalibre=false;
			}
			else {
				celdaAux=tablero.getCelda(fila+i,columna-i);
				if (celdaAux.puedeIngresarPieza(this)){
					posiblesMovimientos.add(celdaAux);
				}
				else {
					celdalibre=false;
				}
			}
			i++;
		}



		celdalibre=true;
		i=1;
		while (celdalibre ){//movimiento disponibles por diagonal izquiero inferior

			if(fila-i <0 ||columna-i < 0){
				celdalibre=false;
			}
			else {
				celdaAux=tablero.getCelda(fila-i,columna-i);
				if (celdaAux.puedeIngresarPieza(this)){
					posiblesMovimientos.add(celdaAux);
				}
				else {
					celdalibre=false;
				}
			}
			i++;
		}




		celdalibre=true;
		i=1;
		while (celdalibre ){ //movimiento disponibles por diagonal derecho inferior

			if(fila-i <0 || columna+i>7){
				celdalibre=false;
			}
			else {
				celdaAux=tablero.getCelda(fila-i,columna+i);
				if (celdaAux.puedeIngresarPieza(this)){
					posiblesMovimientos.add(celdaAux);
				}
				else {
					celdalibre=false;
				}
			}
			i++;
		}


		boolean sePuede=true;
		i=fila;
		while(sePuede) { //mientras se puede avanzar hacia arriba 
			if (i>0) {
				i--;
				celdaAux=tablero.getCelda(i, columna);
				if(celdaAux.puedeIngresarPieza(this)) {
					posiblesMovimientos.add(celdaAux);
				}
				else {
					sePuede=false;
				}
			}
			else {
				sePuede=false;
			}
		}

		sePuede=true;
		i=fila;
		while(sePuede) { //mientras se puede avanzar hacia abajo 
			if (i<7) {
				i++;
				celdaAux=tablero.getCelda(i, columna);
				if(celdaAux.puedeIngresarPieza(this)) {
					posiblesMovimientos.add(celdaAux);
				}
				else {
					sePuede=false;
				}
			}
			else {
				sePuede=false;
			}
		}

		sePuede=true;
		i=columna;
		while(sePuede) { //mientras se puede avanzar hacia derecha
			if (i<7) {
				i++;
				celdaAux=tablero.getCelda(fila, i);
				if(celdaAux.puedeIngresarPieza(this)) {
					posiblesMovimientos.add(celdaAux);
				}
				else {
					sePuede=false;
				}
			}
			else {
				sePuede=false;
			}
		}

		sePuede=true;
		i=columna;
		while(sePuede) { //mientras se puede avanzar hacia izquierda 
			if (i>0) {
				i--;
				celdaAux=tablero.getCelda(fila, i);
				if(celdaAux.puedeIngresarPieza(this)) {
					posiblesMovimientos.add(celdaAux);
				}
				else {
					sePuede=false;
				}
			}
			else {
				sePuede=false;
			}
		}







		return posiblesMovimientos;
	}


}
