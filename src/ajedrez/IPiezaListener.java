package ajedrez;

public interface IPiezaListener {

	public default void piezaMovida(Pieza pieza, Celda celda1, Celda celda2) {
		
	}
	
	public default void piezaComida(Pieza pieza, Celda celda1, Celda celda2) {
		
	}
}
