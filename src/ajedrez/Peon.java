package ajedrez;

import java.util.ArrayList;

import javax.swing.ImageIcon;

public class Peon extends Pieza{
	public Peon(Celda celda, Equipo equipo) {
		super(celda, equipo);
		if(equipo.isEstaArriba())
			this.setIcono(new ImageIcon("D:\\Users\\corre\\eclipse-workspace\\TrabajoPracticoIntegrador\\img\\peonNegro.png"));
		else
			this.setIcono(new ImageIcon("D:\\Users\\corre\\eclipse-workspace\\TrabajoPracticoIntegrador\\img\\peonBlanco.png"));
	}

	@Override
	public ArrayList<Celda> getMovimientosPosibles() {
		ArrayList<Celda> posiblesMovimientos=new ArrayList<Celda>();// se crea un array para poder incluir posibles jugadas
		Tablero tablero=this.getEquipo().getAjedrez().getTablero();
		int fila=this.getCelda().getFila(); // se desglosa la celda que contiene al peon en fila y columna
		int columna=this.getCelda().getColumna();
		Celda celda=new Celda(fila,columna); // se crea una celda para manipular casos

		if(this.getEquipo().isEstaArriba()) {
			if(celda.getFila()==1) {
				for(int avanza=1;avanza<=2;avanza++) {
					celda.setColumna(columna);
					celda.setFila(fila+avanza);
					posiblesMovimientos.add(celda);
					for(int col=-1;col<=1;col++) {
						if(columna+col>=0 && col!=0 && columna+col<=7) {
							if(tablero.getCelda(fila+avanza, columna+col).estaOcupadaEquipoContrario(this.getEquipo())) {
								celda.setColumna(columna+col);
								posiblesMovimientos.add(celda);
							}

						}
					}
				}
			}
			else {
				if(fila+1<=7) {
					celda.setFila(fila+1);
					celda.setColumna(columna);
					posiblesMovimientos.add(celda);
					for(int col=-1;col<=1;col++) {
						if(columna+col>=0 && col!=0 && columna+col<=7) {
							if(tablero.getCelda(fila+1, columna+col).estaOcupadaEquipoContrario(this.getEquipo())) {
								celda.setColumna(columna+col);
								posiblesMovimientos.add(celda);
							}
						}
					}
				}
			}

		}
		else {
			if(celda.getFila()==6) {
				for(int avanza=1;avanza<=2;avanza++) {
					celda.setFila(fila-avanza);
					celda.setColumna(columna);
					posiblesMovimientos.add(celda);
					for(int col=-1;col<=1;col++) {
						if(columna+col>=0 && col!=0 && columna+col<=7) {
							if(tablero.getCelda(fila-avanza, columna+col).estaOcupadaEquipoContrario(this.getEquipo())) {
								celda.setColumna(columna+col);
								posiblesMovimientos.add(celda);
							}

						}
					}
				}
			}
			else {
				if(fila-1>=0) {
					celda.setFila(fila-1);
					celda.setColumna(columna);
					posiblesMovimientos.add(celda);
					for(int col=-1;col<=1;col++) {
						if(columna+col>=0 && col!=0 && columna+col<=7) {
							if(tablero.getCelda(fila-1, columna+col).estaOcupadaEquipoContrario(this.getEquipo())) {
								celda.setColumna(columna+col);
								posiblesMovimientos.add(celda);
							}
						}
					}
				}
			}
		}



		return posiblesMovimientos;
	}






	@Override
	public String nombrePieza() {
		return "Peon";
	}
}
