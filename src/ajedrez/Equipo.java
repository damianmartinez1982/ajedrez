package ajedrez;

import java.util.ArrayList;

public class Equipo {
	private static int contadorComidas=0;
	private static int contadorMovidas=0;
	private String nombre;
	private Ajedrez ajedrez;
	private boolean estaEnJaque;
	private boolean estaArriba;
	private ArrayList<Pieza> piezas;
	private int piezasComidas;
	private int movimientosRealizados;

	public Equipo(boolean arriba) {
		this.estaArriba=arriba;
		this.estaEnJaque=false;
		this.piezas=new ArrayList<Pieza>();
		this.piezasComidas=0;
		this.movimientosRealizados=0;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public Ajedrez getAjedrez() {
		return ajedrez;
	}


	public void setAjedrez(Ajedrez ajedrez) {
		this.ajedrez = ajedrez;
	}


	public boolean estaEnJaque() {
		estaEnJaque=false;
		if(this.getAjedrez().getTablero().quienesMatan(this.getRey()).size()>0) {
			estaEnJaque=true;
			this.estaEnJaque=true;
		}

		return estaEnJaque;
	}


	public void setEstaEnJaque(boolean estaEnJaque) {
		this.estaEnJaque = estaEnJaque;
	}


	public ArrayList<Pieza> getPiezas() {
		return piezas;
	}


	public void agregarPiezas(Pieza pieza) {
		this.piezas.add(pieza);
	}


	public Jugada jugar() {
		ArrayList<Jugada> arrayJugadas= new ArrayList<Jugada>();
		arrayJugadas=this.calcularJugadasPosibles();
		int cantidadJugadas=arrayJugadas.size();
		int jugadaElegida=(int)(Math.random()*(cantidadJugadas-1));
		return arrayJugadas.get(jugadaElegida);
	}


	public void actualizarEquipo() {
		for(int i=0;i<this.getPiezas().size();i++)
		{
			if (this.getPiezas().get(i).isEstaViva()==false) {
				this.getPiezas().remove(i);
			}
		}

	}



	public Rey getRey() {
		for(int i=0; i<this.getPiezas().size();i++) {
			if(this.getPiezas().get(i).getClass()==Rey.class) {
				Rey rey=(Rey) this.getPiezas().get(i);
				return rey;
			}
		}
		return null;
	}




	public ArrayList<Jugada> calcularJugadasPosibles(){

		ArrayList<Jugada> jugadasPosibles=new ArrayList<Jugada>();
		ArrayList<Celda> movimientosPosibles= new ArrayList<Celda>();
		int cantidadPiezas=this.getPiezas().size();
		for(int i=0;i<cantidadPiezas;i++) {
			int cantidadJugadaPieza=this.piezas.get(i).getMovimientosPosibles().size();
			movimientosPosibles=this.piezas.get(i).getMovimientosPosibles();
			for(int a=0;a<cantidadJugadaPieza;a++) {
				int fila=movimientosPosibles.get(a).getFila();
				int columna=movimientosPosibles.get(a).getColumna();
				Jugada jugada=new Jugada(null, 0, 0);
				jugada.setJugada(this.piezas.get(i), fila, columna);
				jugadasPosibles.add(jugada);
			}
		}
		return jugadasPosibles;
	}


	public boolean isEstaArriba() {
		return estaArriba;
	}


	public void setEstaArriba(boolean estaArriba) {
		this.estaArriba = estaArriba;
	}


	public int getPiezasComidas() {
		return piezasComidas;
	}


	public void sumarPiezasComidas() {
		contadorComidas++;
		this.piezasComidas = contadorComidas;
	}


	public int getMovimientosRealizados() {
		return movimientosRealizados;
	}


	public void sumarMovimientosRealizados() {
		contadorMovidas++;
		this.movimientosRealizados =contadorMovidas;
	}



}






